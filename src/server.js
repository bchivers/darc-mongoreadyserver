/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * SERVER SETUP (SERVER AND MIDDLEWARE)
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * IMPORTS
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// server and middleware
const _express 		= require( 'express' );		// the server
const _cors 		= require( 'cors' );		// to avoid request denials in a browser
const _bodyParser 	= require( 'body-parser' ); // handle request bodies
const _https 		= require( 'https' ); 		// encrypted requests

// filesystem
const _fs           = require( 'fs' );

// coordinator
const Coordinator   = require( 'daat-coordinator' );

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * LOCAL VARIABLES
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

var _config = undefined , 
	_log    = undefined , 
	_util   = undefined , 
	_col    = undefined , 
	_logins = undefined , 
	_rbac 	= undefined , 
	_mdb 	= undefined ,
	_server = undefined ,  
	_cloop  = undefined ;

var app = undefined;

// store for modules to laod
var _mods = {};

const corsOptions = {
	"origin" 		 : "*" ,
	"methods" 		 : "GET,HEAD,PUT,PATCH,POST,DELETE" ,
	"allowedHeaders" : [ "Content-Type" , "Accepts" , "Authorization" , "Identity" ]
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * LOCAL ROUTINES
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// boilerplate code for nice logging
const uniformLoggingMiddleware = ( req , res , next ) => { 

    const start = _util.now();    // request start time (server time)
    req.key = _util.getKey();  // request response "key" (random)

    // log that the request happened
    _log.info( "REQLOG,"  + req.key 
					+ "," + req.method.toUpperCase()
                    + "," + req.url
                    + "," + start.toFixed(3) );

    // overwrite response object's send function to customize response logging
	const send_ = res.send;
	res.send = ( object ) => { 
		const time = _util.now();
		_log.info(  "RESLOG," + req.key 
						+ "," + req.method.toUpperCase()
						+ "," + req.url
						+ "," + res.statusCode
						+ "," + start.toFixed(3) 
						+ "," + time.toFixed(3) 
						+ "," + (time-start).toFixed(3) );
		send_.call( res , object );
	};

	// move on
	next();

}

// wrapper for callbacks with some form of basic authorization
const authorizedCallback = ( callback , rights , req , res ) => {
	if( ! req.rights[rights] ) { res.status(403).send(); }
	else { callback( req , res ); }
};

// load routes defined in a "module"
const loadModuleRoutes = ( mod ) => {
	if( mod.routes ) {
		Object.keys( mod.routes ).forEach( ( k ) => {
			let route    = mod.routes[k].route;
			let callback = mod.routes[k].callback;
			if( mod.routes[k].rights ) {
				callback = authorizedCallback.bind( this , mod.routes[k].callback , mod.routes[k].rights );
			} else {
				callback = mod.routes[k].callback;
			}
			if( route && callback && mod.routes[k].methods ) {
				if( ! Array.isArray( mod.routes[k].methods ) ) {
					mod.routes[k].methods = [ mod.routes[k].methods ];
				}
				mod.routes[k].methods.forEach( method => app[method](route,callback) );
			}
		} );
	}
}

// Is using Coordinator ok with SYNCHRONOUS loads? Or does it break down?
// would promises help?
const executeLoadModule = ( data , failure , warning , success ) => {
	_log.info( "  loading module " + data.name )
	try {
		var result = require( `${data.file}` )( data.ctx );
		data.ctx[data.name] = result;
		if( success ) { success( result ); } else { return result; }
	} catch( err ) {
		console.log( err );
		if( success ) { success(); }
	}
}

// load checks (for the check loop) that are defined in a "module"
const loadModuleChecks = ( mod ) => {
	if( mod.checks ) {
		Object.keys( mod.checks ).forEach( ( k ) => {
			let prereqs = [ "reloadMongoDB" ];
			if( "prereqs" in mod.checks[k] && mod.checks[k].prereqs ) {
				if( Array.isArray( mod.checks[k].prereqs ) ) {
					prereqs = prereqs.concat( mod.checks[k].prereq );
				} else {
					prereqs.push( mod.checks[k].prereqs );
				}
			}
			_cloop.addStage( {
				key     : k , 
				execute : mod.checks[k].execute , 
				prereqs : prereqs , 
			} );
		} );
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * SERVER SETUP (SERVER AND MIDDLEWARE)
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const loadServer = ( ) => {

	return new Promise( ( resolve , reject ) => {

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * 
		 * 
		 * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		// stop (clear) the checkloop interval if it is running
		if( _cloop ) { _cloop.stopCheckLoop(); }

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * 
		 * BUILD THE "APP"
		 * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		// the express app
		app = _express();

		// need to include this to not have CORS issues, adding custom headers we should allow in requests
		app.use( _cors( corsOptions ) );

		// body parsing middleware
		app.use( _bodyParser.json() );
		app.use( _bodyParser.urlencoded( { extended : false } ) );

		// uniform logging middleware - execute __before__ authorization checks so we 
		// capture __all__ requests
		app.use( uniformLoggingMiddleware );

		// define the authorization data (we need to EXPLICITLY REFERENCE the logins module, 
		// unless we actually embedd that code here)
		app.use( _logins.isAuthorized );

		// when we plug in mongodb-based RBAC, add this middleware
		// app.use( _mdbrbac.getRoles );
		
		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * 
		 * GENERIC ROUTES, ABOUT SERVER
		 * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		/** 
		 *  
		 * @api {get} / Get an info message
		 * @apiVersion 0.1.0
		 * @apiName info
		 * @apiGroup 
		 * 
		 * @apiDescription Get this and the next academic quarter, based on the current date. 
		 * 
		 * @apiPermission open
		 *  
		 */

		app.get( '/' , ( req , res ) => { 
			res.send( `${_pkg.name} (${_pkg.version}): ${_pkg.description}.\n` );
		} );

		/** 
		 *  
		 * @api {get} /fake/error Fake an error
		 * @apiVersion 0.1.0
		 * @apiName fakeerr
		 * @apiGroup 
		 * 
		 * @apiDescription Fake an uncaught server error (DEBUGGING ONLY)
		 * 
		 * @apiPermission admin
		 *  
		 */

		app.get( '/fake/error' , ( req , res ) => { 
			throw new Error( 'fake error called.' );
		} );

		/**
		 * 
		 * @api {get} /options Get server options
		 * @apiVersion 0.1.0
		 * @apiName getOptions
		 * @apiGroup 
		 * 
		 * @apiDescription Get the current configuration and options
		 * 
		 * @apiPermission admin
		 * 
		 */
		app.get( '/options' , ( req , res ) => { 
			res.json( _config );
		} );

		/**
		 * 
		 * @api {post} /options Update server options
		 * @apiVersion 0.1.0
		 * @apiName updateOptions
		 * @apiGroup 
		 * 
		 * @apiDescription Update the current configuration and options. Note this will not have an effect
		 * 				   unless the server is rebooted.
		 * 
		 * @apiPermission admin
		 * 
		 */
		app.post( '/options' , ( req , res ) => { 
			// update options using request body
			if( ! _util.vdk( "body" , req ) ) { 
				res.status(400).send( "POST /options expects a request body." );
			}
			try {
				let _id = _util.mdboid( _config._id );
				_col.options.updateOne( 
					{ _id : _util.mdboid( _config._id ) } , 
					{ $set : req.body } , 
					( err , result ) => {
						if( err ) { res.status(500).send( "Failed to update options: " + err.toString() ); }
						else { res.send(); }
					}
				);
			} catch(err) {
				res.status(500).send( "Failed to update options: " + err.toString() ); 
			}
		} );

		/**
		 * 
		 * @api {post} /reboot Reboot server
		 * @apiVersion 0.1.0
		 * @apiName reboot
		 * @apiGroup 
		 * 
		 * @apiDescription Reboot the connection to MongoDB and the server
		 * 
		 * @apiPermission admin
		 * 
		 */
		app.post( '/reboot' , ( req , res ) => { 
			_server.close();
			_mdb.reloadMongoDB()
				.then( result  => { 
					_config = result.config; _col = result.col; // other settings stay the same
					loadServer()
						.then( () => { res.send( "reboot successful.\n" ); } )
						.catch( err => { res.status(500).send( "reboot failed: " + err.toString() ); } )
				} ).catch( err => { res.status(500).send( "reboot failed: " + err.toString() ); } );
		} );

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * 
		 * LOAD ANY ROUTES DEFINED IN MODULES
		 * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		// ... uh... loading routes
		Object.keys( _mods ).forEach( m => loadModuleRoutes( _mods[m] ) );

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * 
		 * ERROR HANDLING
		 * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		// define middleware that will help us log otherwise uncaught errors
		// NOTE: express itself has error handling channels, probably better to rely on those?
		// Or IS THIS THOSE? I forget. I think by placing this after route calls it is. 
		app.use( ( err , req , res , next ) => {
			
			res.status(500).send( err.toString() );

			// log the error
			_log.error( err );
			_log.info( `ERRLOG,${req.key},${err.toString()}` );

			// here we can also store errors in MongoDB (unless the errors are _with_ MongoDB)
			// what's a backup? S3? pretty reliable. 

			try {
				_col.errors.insertOne( err , ( error , result ) => {
					if( error ) { /* backup here */ }
				} );
			} catch( error ) {
				/* backup here */
			}

			// here we can post error notification to slack or via email, if we like


			// proceed
			next();

		} );

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * 
		 * START SERVER
		 * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		// what to do when server is actually listening
		const onListen = () => { 
			_log.info( "Listening on port " + _config.port ); 
			process.on( 'exit' , () => { server.close(); } )
		}

		// start server
		let useSSL = _config.ssl && _config.ssl.key && _config.ssl.cert && _config.ssl.chain;
		if( useSSL ) {
			_server = _https.createServer( {
			    key 	: _fs.readFileSync( _config.ssl.key   ) ,
			    cert 	: _fs.readFileSync( _config.ssl.cert  ) ,
			    ca 		: _fs.readFileSync( _config.ssl.chain ) ,
			    requestCert : false ,
			    rejectUnauthorized : false
			} , app );
			_server.listen( _config.port , onListen );
		} else {
			_server = app.listen( _config.port , onListen );
		}
		resolve( _server );

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * 
		 * DEFINE AND START CHECKLOOP
		 * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
		
		// check loop? How are we reloading? 
		// 
		// each module can have a "checks" field, like it has "routes"

		/*
		// reload the database connection FIRST... how to make sure this always 
		// happens FIRST? add as a prereq to any other call? incorporate "blocks"
		// into the checkloop? Maybe some checks don't care about MongoDB...
		_cloop.addStage( {
			key     : "reloadMongoDB" , 
			execute : _mdb.reloadMongoDB , 
		} );
		*/

		// load any checks defined in the modules
		Object.keys( _mods ).forEach( m => loadModuleChecks( _mods[m] ) );

		// start the check loop
		_cloop.startCheckLoop();

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * 
		 * 
		 * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	} );

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * MODULE EXPORTS
 * 
 * The actual export is a function that can be called with "context" data to initiate setup
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports = ( ctx , modules ) => {

	// basics
	_pkg = ctx.pkg; _config = ctx.cfg; _log = ctx.log; 
	_util = ctx.utl; _col = ctx.col; _mdb = ctx.mdb;

	// construct "logins" module for authorizing requests
	_logins = require( './logins.js' )( ctx );

	// modules argument not passed? if not set default
	modules = modules ? modules : ctx.dir + "/modules";

	// load "prereqs", if it exists. this will let us order modules we load. 
	// NOTE: better read from the file itself, than a separate file...  maybe search for 
	// 
	// 	// @prereq(s)
	//
	// or 
	// 
	// /* ... 
	//	@prereq(s) 
	//	... */
	// 
	// style tags in comments in the files? 
	let prereqs = {}; try { prereqs = require( modules + "/prereqs.json" ); } catch( err ) { }

	// import checkloop now that _config and _log are defined
	_cloop = require( './checkloop.js' )( _config , _log );
	_cloop.quiet();

	return new Promise( ( resolve , reject ) => {

		// ok, read the "modules" subdirectory... any *.js file in there should be loadable
		_fs.readdir( modules , ( err , items ) => {

			// don't do work for empty directories
			if( items ) { 

				// get a list of all the names of '.js' files
				let names = items.filter( m => ( /\.js$/.test( m ) ) ).map( m => m.replace( /.js$/ , '' ) );

				// load modules "in order" using a Coordinator
				var C = new Coordinator();

				// stages for each named module
				names.forEach( n => { 
					C.addStage( {
						key     : n , 
						execute : executeLoadModule , 
						prereqs : ( n in prereqs ? prereqs[n] : [] ) , 
						data    : { name : n , file : `${modules}/${n}.js` , ctx : ctx }
					} );
				} );

				// run, without printing anything
				C.quiet();
				C.run( 
					reject , 
					( res ) => { 
						_mods = res; // the results of this coordinator are the modules
						loadServer( ).then( resolve ).catch( reject ); // load server only * after * modules loaded
					}
				);

			} else {
				_log.info( "Empty modules directory passed to server. Nothing to add." );
				loadServer( ).then( resolve ).catch( reject ); // load server only * after * modules loaded
			}

		});

	} );

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * Copyright 2018+, Stanford GSB CIRCLE RSS
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */