
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * 
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const _axios = require( 'axios' );

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * 
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

var _config = undefined , 
	_log 	= undefined , 
	_util 	= undefined ;

var loginServer = undefined ;

var enforcingAuthorization = false;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * 
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// get authorization from headers, if it exists
const authorization = ( req ) => (
	"headers" in req 
		? ( ( "Authorization" in req.headers 
				? req.headers.Authorization 
				: ( "authorization" in req.headers 
					? req.headers.authorization
					: "unknown" ) ) )
        : "unknown" 
);

// get identity from headers, if it exists
const identity = ( req ) => (
	"headers" in req 
		? ( ( "Identity" in req.headers 
				? req.headers.Identity 
				: ( "identity" in req.headers 
					? req.headers.identity
					: "unknown" ) ) )
        : "unknown" 
);

// check a token against login server defined in config
const checkToken = ( token ) => {
	return new Promise( ( resolve , reject ) => {
		loginServer.get( "/" + token )
			.then( response => { resolve( response.data.uid ); } )
			.catch( error => {
				if( error.response ) {
					reject( { status : error.response.status , data : error.response.data } );
				} else {
					reject( { status : 0 , data : "unknown error" } );
				}
			} );
	} );
}

// evaluate whether a request is authorized or not
const isAuthorized = ( req , res , next ) => {

	req.authorization = authorization( req );
	req.identity = identity( req );
	if( ! req.rights ) { req.rights = { authed : false }; }

	// logging line
	_log.info( `WHOLOG,${req.key},${req.identity},${req.authorization}` );

	// authorization and identity checking (shouldn't we store this
	// as a boolean?)
	if( enforcingAuthorization ) {
		_log.info( "Using (well, _enforcing_) authorization" );
		checkToken( req.authorization )
			.then( ( ) => { 
				req.rights.authed = true;
				next();
			} ).catch( ( err ) => { 
				_log.error( err );
				_log.info( "Authorization check error: " + err.toString() ); 
				next();
			} );
	} else {
		req.rights.authed = true;
		next();
	}

};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * 
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports = ( ctx ) => {

	// basics
	_config = ctx.cfg;
	_log 	= ctx.log; 
	_util 	= ctx.utl; 

	// create the login server URL
	if( _config.login ) {

		var loginServerURL = ( _config.login.protocol ? _config.login.protocol : "https" ) + "://"
								+ _config.login.host
								+ ( _config.login.port ? ":" + _config.login.port : "" )
								+ "/" + ( _config.login.path ? _config.login.path : "" );

		// create a wrapper object
		loginServer = _axios.create( {
			baseURL : loginServerURL , 
			timeout : 1000 , // the idea is that this is called for EVERY request, if auth is on, and thus slowness here is a bigger problem
		} );

		enforcingAuthorization = /([Yy](es)*|[Tt](rue)*)/.test( _config.enforce_authorization );

	}

	// return calls the "require"ing code can use
	return { 
		isAuthorized : isAuthorized , 
	};

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * Copyright 2019+, Stanford GSB CIRCLE RSS
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */