/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * MONGODB READY SERVER
 * 
 * Copyright Stanford GSB DARC Team 
 * 
 * Created by W. Ross Morrow, Research Computing Specialist
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

'use strict';

var _pkg 	 = null , 
	_config  = null , 
	_log 	 = null , 
	_util 	 = null , 
	_mongodb = null , 
	_moddir  = null , 
	_server  = null ;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * MODULE EXPORTS
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const loadServer = ( modules ) => {

	return new Promise( ( resolve , reject ) => {

		// returns a Promise that resolves with a config updated with options and a MongoDB 
		// collections references object for the specified MongoDB collection names. 
		_mongodb.reloadMongoDB( )
			.then( result => { 

				// HERE we can setup a server with the defined configuration and collections
				let ctx = {
					dir : /(.*)\/[^\/]*$/.exec( process.mainModule.filename )[1] , 
					pkg : _pkg , 
					cfg : result.config , // reloadMongoDB may modify _config with options
					log : _log , 
					utl : _util , 
					col : result.col , 
					mdb : _mongodb , 
				};

				// server.js will need to know where to find "modules". That is, 
				// files with routines, routes and their handlers, and any checks. 
				// if we want a "forkable" repository, we can just standardize that to 
				// be in src/modules. But if we want an "importable" module, we can't. 
				// we have to pass an argument that defines where the modules are
				// in the project importing the server code. 
				require( './server.js' )( ctx , modules )
					.then( s => { ctx.srv = s; resolve( ctx ); } )
					.catch( reject );

			} ).catch( reject );

	} );

}

module.exports = ( modules , optargs , checks ) => {

	// we can assign to this "module" variable because the "modules" to load shouldn't change
	_moddir = modules;

	// is there better then/catch syntax than this explicit Promise wrapping?
	return new Promise( ( resolve , reject ) => {

		require( 'darc-mongoreadyserverconfig' )( optargs , checks )
			.then( c => { 

				_config  = c; // passed configuration from the configuration parser
				_log 	 = require( './logger.js' )( c ); // this sets up the logs
				_util 	 = require( './utilities.js' )( _log ); // utilities used frequently, across submodules
				_mongodb = require( './mongodb.js' )( c , _log , _util ); // load mongodb with these values

				_pkg = require( `${process.cwd()}/package.json` );

				_log.info( `* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ` );
				_log.info( `* ${_pkg.name} (v ${_pkg.version}) starting...` );
				_log.info( `* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ` );

				// actually load the server
				loadServer( modules ).then( resolve ).catch( reject );

			} ).catch( reject );

	} );

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * Copyright 2020+, Stanford GSB DARC Team
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */